var emojiButtons = document.getElementById("emoji-wrapper").querySelectorAll(".emoji");
var count = 0;
var isSender = true;
const wrapper = document.getElementById("emoji-wrapper");
const spawner = document.getElementById("emoji-spawner");
const receiver = document.getElementById("emoji-receive");
const sender = document.getElementById("emoji-send");
const emojiList = ["❤", "🔥", "😎", "🤮", "💃"];

wrapper.addEventListener('click', function() {
    const isButton = event.target.nodeName === 'BUTTON';
    isSender = false;

    if(!isButton) { 
        return;
    }

    const emoji = event.target.innerHTML;
    spawnEmoji(emoji);
    isSender = true;
});

function spawnEmoji(emoji) {
    // spawn emoji
    const floater = document.createElement('span');
    floater.innerHTML = emoji;
    floater.addEventListener("animationend", function() {
        floater.remove();
    })
    position = 'left: ' + Math.floor(Math.random() * 70) + "px";
    floater.setAttribute('style', position);
    spawner.appendChild(floater);

    // if emoji sent by sender, do not record it
    if (isSender) {
        return;
    }

    console.log("adding to sender");

    // record in sender
    /*if (count > 5) {
        // don't clog up sender
        sender.innerHTML = '';
        count = 0;
    }*/
    sender.innerHTML += emojiList.indexOf(emoji) + ',';
    //count++ 
}

// Do this every second
setInterval(function(){
	// Convert received emojis
    var emojis = receiver.innerHTML
    sender.innerHTML = '';
    if (!emojis) {
        return;
    }
    var index = emojis.indexOf(",")
    var spawn = emojiList[parseInt(emojis.slice(0, index))];
    receiver.innerHTML = emojis.slice(index + 1);
    spawnEmoji(spawn)

}, 600);
