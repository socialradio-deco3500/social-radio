/*
Adapted from Rumyra (Mozilla) example code.
https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Using_Web_Audio_API
*/

console.clear();

// instigate our audio context

// for cross browser
const AudioContext = window.AudioContext || window.webkitAudioContext;
const audioCtx = new AudioContext();
const gainNode = audioCtx.createGain();
gainNode.gain.setValueAtTime(1, audioCtx.currentTime); // volume is full

// load some sound
const audioElement = document.querySelector('audio');
const track = audioCtx.createMediaElementSource(audioElement);
var queue = [];
var connected = false;
var timeRemaining = 0;
var onBreak = false;
var justPlayedASong = false;

const isReceiver = document.getElementById('radio-wrapper').className === 'receive';

const connectButton = document.getElementById('connect-button');
const playButton = document.querySelector('.tape-controls-play');
const unmutedHTML = "<span>Unmute</span>";
const mutedHTML = "<span>Mute</span>";
const waitingHTML = "<span>waiting...</span>";

const skipButton = document.getElementById('controls-skip');
const queueButton = document.getElementById('controls-queue-submit');
const queueInput = document.getElementById('controls-queue-input');
const queueData = document.getElementById('data-queue');
const timeData = document.getElementById('data-timestamp');
const breakCounter = document.getElementById('time-remaining');

const sendSongData = document.getElementById('data-sent');
const getSongData = document.getElementById('data-received');
var currentSongID = -1;

// unsuspend audio -- NECESSARY??
connectButton.addEventListener('click', function() {
    // check if context is in suspended state (autoplay policy)
    if (audioCtx.state === 'suspended') {
		audioCtx.resume();
	}
})

// play pause audio
playButton.addEventListener('click', function() {
    connected = getSongData.innerHTML;

    if (isReceiver && connected) {
        playReceiveRadio(this);
        
    } else if (!isReceiver) {
        playSendRadio(this);
    }

	let state = this.getAttribute('aria-checked') === "true" ? true : false;
	this.setAttribute( 'aria-checked', state ? "false" : "true" );

}, false);

// if track ends
audioElement.addEventListener('ended', () => {
    console.log("Song ended!");
	queue.shift();
    queueData.innerHTML = queue;
	startBreak(40);
}, false);

// connect tracks
track.connect(gainNode);
gainNode.connect(audioCtx.destination);


function playReceiveRadio(This) {
    // check if context is in suspended state (autoplay policy)
	if (audioCtx.state === 'suspended') {
		audioElement.currentTime = timeData.innerHTML;
		audioCtx.resume();
	}

    if (This.dataset.playing === 'false') {
		audioElement.currentTime = timeData.innerHTML;
		if (connected) { 
            play();
            playButton.innerHTML = mutedHTML;
            This.dataset.playing = 'true';
        }
	// if track is playing pause it
	} else if (This.dataset.playing === 'true') {
		audioElement.currentTime = timeData.innerHTML;
		if (connected) { 
            audioElement.pause();
            playButton.innerHTML = unmutedHTML;
            This.dataset.playing = 'false';
        }
	}
}

function playSendRadio(This) {
    if (audioCtx.state === 'suspended') {
		audioCtx.resume();
        wasSuspended = true;
	}

    if (This.dataset.playing === 'false') {
        gainNode.gain.setValueAtTime(1, audioCtx.currentTime); // volume is full
        This.innerHTML = mutedHTML;
		This.dataset.playing = 'true';

	// if track is playing pause it
	} else if (This.dataset.playing === 'true') {
        gainNode.gain.setValueAtTime(0, audioCtx.currentTime); // volume is muted
        This.innerHTML = unmutedHTML;
		This.dataset.playing = 'false';
	}
}

// Do this every second
setInterval(function(){
	// send data
	if (audioElement && !isReceiver) {
		timeData.innerHTML = audioElement.currentTime;
        sendSongData.innerHTML = currentSongID; // song id
	}

    if (queueData.innerHTML == false) {
        queueData.innerHTML = "Nothing is playing - please add a track to play!<br>";
    }

    if (0 < timeRemaining && onBreak) {
        if (!isReceiver) {
            timeRemaining -= 0.5;
            breakCounter.innerHTML = Math.ceil(timeRemaining);
        } else {
            timeRemaining = breakCounter.innerHTML;
        }
    } else if (onBreak) {
        // end break
        breakCounter.setAttribute('style', 'display: none');
        timeData.innerHTML = -1;
        onBreak = false;
        playNext();
    }
    
    // retrieve song data
    if (isReceiver) {
        if (currentSongID != getSongData.innerHTML) {
            currentSongID = getSongData.innerHTML;
            queue = queueData.innerHTML.split(",");
            var wasPlaying = playButton.dataset.playing === 'true';
            audioElement.setAttribute("src", queue[0]);
            audioElement.currentTime = timeData.innerHTML;
            //sendState.innerHTML = stateStreamUpdated;

            if (wasPlaying) {
                play();
            } else {
                audioElement.pause();
            }
        }
    }

}, 500);

// Update queue
queueButton.addEventListener('click', function () {
	var music = queueInput.value;
	sendMessageBox.value = "";
	queue.push(music);
	queueData.innerHTML = queue;
	if (queue.length === 1) {
		playNext();
	}
});

skipButton.addEventListener('click', function() {
	queue.shift();
	queueData.innerHTML = queue;
	playNext();
});

function startBreak(time) {
    onBreak = true;
    timeRemaining = time;
    breakCounter.innerHTML = timeRemaining
    breakCounter.setAttribute('style', 'display: block');
    audioElement.pause();
}

function playNext() {
	if (!queue || onBreak) {
		audioElement.pause();
	} else {
		audioElement.setAttribute("src", queue[0]);
		play();
        currentSongID++;
	}
}

function play() {
    if (!onBreak) {
        audioElement.play();
    }
}
